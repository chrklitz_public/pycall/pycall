#include <gtest/gtest.h>

#include <pycall/core/typelist.hpp>

TEST(TYPELIST, GENERAL) {
  using tl1_0 = pycall::typelist<int, float, char>;
  using tl1_1 = typename tl1_0::tail;
  using tl1_2 = typename tl1_1::tail;

  using tl1_3 = typename tl1_2::tail; // tail of size-1 typelist is empty
  using tl1_4 = typename tl1_3::tail; // tail of size-0 typelist is pycall::none

  ASSERT_TRUE(!static_cast<bool>(tl1_0::empty));
  ASSERT_TRUE((std::is_same<typename tl1_0::head, int>::value));

  ASSERT_TRUE(!static_cast<bool>(tl1_1::empty));
  ASSERT_TRUE((std::is_same<typename tl1_1::head, float>::value));

  ASSERT_TRUE(!static_cast<bool>(tl1_2::empty));
  ASSERT_TRUE((std::is_same<typename tl1_2::head, char>::value));

  ASSERT_TRUE((std::is_same<tl1_3, pycall::typelist<>>::value));
  ASSERT_TRUE((std::is_same<tl1_4, pycall::none>::value));

  using tl2 = pycall::typelist<>;
  ASSERT_TRUE(static_cast<bool>(tl2::empty));


}

