#ifndef PYCALL_TUPLE_WRAPPER_HPP
#define PYCALL_TUPLE_WRAPPER_HPP

#include <utility>
#include <memory>
#include <functional>
#include "pycall_connection_traits.hpp"
#include "pycall_caster_traits.hpp"
#include "pycall_stream.hpp"

#include <iostream>

namespace pycall {

namespace detail {
// Write the tuple elements to stream (already terminal types)
template<std::size_t index = 0, typename... Args>
inline typename std::enable_if<index == sizeof...(Args), void>::type
write_tuple_to_stream(pycall::stream &stream, std::tuple<Args...>& tuple) { }
template<std::size_t index = 0, typename... Args>
inline typename std::enable_if<(index < sizeof...(Args)), void>::type
write_tuple_to_stream(pycall::stream &stream, std::tuple<Args...>& tuple) {
  stream << std::get<index>(tuple);
  std::cout << std::get<index>(tuple) << ", ";
  write_tuple_to_stream<index + 1, Args...>(stream, tuple);
}


// Read the tuple elements from stream (already terminal types)
template<std::size_t index = 0, typename... Args>
inline typename std::enable_if<index == sizeof...(Args), void>::type
read_tuple_from_stream(pycall::stream &stream, std::tuple<Args...>& tuple) { }
template<std::size_t index = 0, typename... Args>
inline typename std::enable_if<(index < sizeof...(Args)), void>::type
read_tuple_from_stream(pycall::stream &stream, std::tuple<Args...>& tuple) {
  stream >> std::get<index>(tuple);
  read_tuple_from_stream<index + 1, Args...>(stream, tuple);
}



// Convert a tuple by casting each of its elements to its terminal-type
template<typename Mapping, std::size_t index = 0, typename... Args>
inline typename std::enable_if<index == sizeof...(Args), void>::type
tuple_cast_to_terminal(const std::tuple<Args...>& from, std::tuple<typename pycall::terminal_type<Mapping, Args>::type...> &to) { }
template<typename Mapping, std::size_t index = 0, typename... Args>
inline typename std::enable_if<(index < sizeof...(Args)), void>::type
tuple_cast_to_terminal(const std::tuple<Args...>& from, std::tuple<typename pycall::terminal_type<Mapping, Args>::type...> &to) {
  std::get<index>(to) = pycall::cast_to_terminal<Mapping>(std::get<index>(from));
  tuple_cast_to_terminal<Mapping, index + 1, Args...>(from, to);
}
template<typename Mapping, std::size_t index = 0, typename... Args>
inline typename std::enable_if<(index < sizeof...(Args)), void>::type
tuple_cast_to_terminal(std::tuple<Args...>&& from, std::tuple<typename pycall::terminal_type<Mapping, Args>::type...> &to) {
  // moving 'from' twice should be fine because std::get only invalidates the element at the specified index.
  // The recursive call to 'tuple_cast_to_terminal' will not touch that element at that index again.´
  std::get<index>(to) = pycall::cast_to_terminal<Mapping>(std::get<index>(std::move(from)));
  tuple_cast_to_terminal<Mapping, index + 1, Args...>(std::move(from), to);
}



// Convert a tuple by casting each of its elements from its terminal-type to the specified type in Args
template<typename Mapping, std::size_t index = 0, typename... Args>
inline typename std::enable_if<index == sizeof...(Args), void>::type
tuple_cast_from_terminal(std::tuple<typename pycall::terminal_type<Mapping, Args>::type...>&& from, std::tuple<Args...> &to) { }
template<typename Mapping, std::size_t index = 0, typename... Args>
inline typename std::enable_if<(index < sizeof...(Args)), void>::type
tuple_cast_from_terminal(std::tuple<typename pycall::terminal_type<Mapping, Args>::type...>&& from, std::tuple<Args...> &to) {
  using from_t = std::tuple<typename pycall::terminal_type<Mapping, Args>::type...>;
  std::get<index>(to) = pycall::cast_from_terminal<Mapping, typename std::remove_reference<decltype(std::get<index>(to))>::type>(std::get<index>(std::forward<from_t>(from)));
  tuple_cast_from_terminal<Mapping, index + 1, Args...>(std::forward<from_t>(from), to);
}

} // end namespace detail
} // end namespace detail

namespace pycall {

class tuple_wrapper {
public:
  virtual void write_to_stream(pycall::stream &s) = 0;
  virtual void read_from_stream(pycall::stream &s) = 0;
  virtual ~tuple_wrapper() = default;
};


template<typename Mapping, typename... Args>
class tuple_wrapper_typed : public pycall::tuple_wrapper {
  using tuple_t = std::tuple<typename pycall::terminal_type<Mapping, Args>::type...>;
public:
  tuple_wrapper_typed(tuple_t &&p)
    : tuple(std::forward<tuple_t>(p))
  { }

  tuple_wrapper_typed() = default;

  tuple_wrapper_typed(tuple_wrapper_typed &&other)
  {
    *this = std::forward<tuple_wrapper_typed>(other);
  }

  tuple_wrapper_typed &operator=(const tuple_wrapper_typed &other) {
    if (this != &other) {
      tuple = other.tuple;
    }
  }
  tuple_wrapper_typed &operator=(tuple_wrapper_typed &&other) {
    if (this != &other) {
      tuple = std::move(other.tuple);
    }
  }

  ~tuple_wrapper_typed() override = default;

  operator tuple_t() {
    return tuple;
  }

  virtual void write_to_stream(pycall::stream &strm) override {
    strm << static_cast<uint64_t>(std::tuple_size<tuple_t>{});
    std::cout << "write tuple: ";
    pycall::detail::write_tuple_to_stream(strm, tuple);
    std::cout << std::endl;
  }

  virtual void read_from_stream(pycall::stream &strm) override {
    // makes valid entries even if there are shared pointers (needs Args entries to be default-constructible)
    tuple = std::make_tuple(pycall::cast_to_terminal<Mapping>(Args())...);

    uint64_t size;
    strm >> size;
    if (size != sizeof...(Args))
      throw std::invalid_argument("size mismatch between the tuple from stream and the given tuple type");
    pycall::detail::read_tuple_from_stream(strm, tuple);
  }

  tuple_t tuple;
};



template<typename Mapping>
struct tuple_wrapper_caster
{
  template<typename... Args>
  static void upcast(const std::tuple<Args...> &from, std::shared_ptr<pycall::tuple_wrapper> &to) {
    std::tuple<typename pycall::terminal_type<Mapping, Args>::type...> toTuple;
    pycall::detail::tuple_cast_to_terminal<Mapping>(from, toTuple);
    std::cout << "upcast lvalue" << std::endl;
    pycall::tuple_wrapper_typed<Mapping, Args...> *temp
        = new pycall::tuple_wrapper_typed<Mapping, Args...>(std::move(toTuple));

    to = std::shared_ptr<pycall::tuple_wrapper>(temp);
  }
  template<typename... Args>
  static void upcast(std::tuple<Args...> &&from, std::shared_ptr<pycall::tuple_wrapper> &to) {
    std::tuple<typename pycall::terminal_type<Mapping, Args>::type...> toTuple;
    pycall::detail::tuple_cast_to_terminal<Mapping>(std::move(from), toTuple);
    std::cout << "upcast rvalue" << std::endl;
    pycall::tuple_wrapper_typed<Mapping, Args...> *temp
        = new pycall::tuple_wrapper_typed<Mapping, Args...>(std::move(toTuple));

    to = std::shared_ptr<pycall::tuple_wrapper>(temp);
  }

  template<typename... Args>
  static void downcast(const std::shared_ptr<pycall::tuple_wrapper> &from, std::tuple<Args...> &to) {
    std::cout << "downcast lvalue" << std::endl;
    pycall::tuple_wrapper_typed<Mapping, Args...> &temp = dynamic_cast<pycall::tuple_wrapper_typed<Mapping, Args...> &>(*from);
    pycall::detail::tuple_cast_from_terminal<Mapping>(temp, to);
  }

  template<typename... Args>
  static void downcast(std::shared_ptr<pycall::tuple_wrapper> &&from, std::tuple<Args...> &to) {
    std::cout << "downcast rvalue" << std::endl;
    pycall::tuple_wrapper_typed<Mapping, Args...> &temp = dynamic_cast<pycall::tuple_wrapper_typed<Mapping, Args...> &>(*std::move(from));
    pycall::detail::tuple_cast_from_terminal<Mapping>(temp, to);
  }
};


} // end namespace pycall

inline pycall::stream &operator<<(pycall::stream &s, std::shared_ptr<pycall::tuple_wrapper> pair) {
  pair->write_to_stream(s);
  return s;
}
inline pycall::stream &operator>>(pycall::stream &s, std::shared_ptr<pycall::tuple_wrapper> &pair) {
  pair->read_from_stream(s);
  return s;
}


#endif // PYCALL_TUPLE_WRAPPER_HPP
