#ifndef TYPESTRING_TREE_H
#define TYPESTRING_TREE_H

#include <vector>
#include <string>
#include <memory>
#include <ostream>

#include "pycall_stream.hpp"

namespace pycall {


class typestring_tree
{
private:
  struct Node {
    std::string typestring;
    std::shared_ptr<typestring_tree> child;
  };

public:
  typestring_tree &push_back(const std::string &typestring) {
    children.emplace_back(Node{typestring, std::shared_ptr<typestring_tree>(new typestring_tree())});
    return *children.back().child;
  }

  std::vector<std::string> type_strings() const {
    std::vector<std::string> ts;
    for (const auto &n : children) {
      ts.push_back(n.typestring);
    }
    return ts;
  }

  std::vector<std::pair<std::string, const typestring_tree &>> entries() const {
    std::vector<std::pair<std::string, const typestring_tree &>> es;
    for (const auto &n : children) {
      es.push_back(std::pair<std::string, const typestring_tree &>(n.typestring, *n.child));
    }
    return es;
  }

  bool operator==(const typestring_tree &other) {
    if (children.size() != other.children.size())
      return false;

    for (size_t i = 0; i < children.size(); ++i) {
      if ((children[i].typestring != other.children[i].typestring) || (*children[i].child != *other.children[i].child))
        return false;
    }
    return true;
  }
  bool operator!=(const typestring_tree &other) {
    return !(*this == other);
  }

private:
  std::vector<Node> children;
};

} // end namespace pycall

inline std::ostream &operator<<(std::ostream &os, const pycall::typestring_tree &t) {
  os << "typestring_tree[";
  const auto &es = t.entries();
  for (const auto &e : es) {
    os << "(" << e.first << ", " << e.second << ")";
    if (&e != &es.back())
      os << ", ";
  }
  os << "]";
  return os;
}

inline pycall::stream &operator<<(pycall::stream &stream, const pycall::typestring_tree &t) {
  const auto es = t.entries();
  stream << static_cast<uint64_t>(es.size());
  for (const auto &e : es) {
    stream << e.first << e.second;
  }
  return stream;
}
inline pycall::stream &operator>>(pycall::stream &stream, pycall::typestring_tree &t) {
  uint64_t size;
  stream >> size;
  for (uint64_t i = 0; i < size; ++i) {
    std::string ts;
    pycall::typestring_tree child;
    stream >> ts >> child;
    t.push_back(ts) = child;
  }
  return stream;
}
#endif // TYPESTRING_TREE_H
