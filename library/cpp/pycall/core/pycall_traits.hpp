/** \file pycall_traits.hpp
 * \brief provides macros for defining type connections. It also includes the other pycall_<x>_traits.hpp files.
 */

#ifndef PYCALL_TRAITS_HPP
#define PYCALL_TRAITS_HPP

#include "pycall_caster_traits.hpp"
#include "pycall_connection_traits.hpp"
#include "typelist.hpp"

// TODO: Make a static_assert checking if there are overloads for stream_write/read for class_name!
// terminal types have a type_string attached to it. This type_string
// is a unique key which identifies a python type (using locate(type_string)) or
// a user-defined read/write stream-function on the python side.
// There must be overloads for the C++ stream read/write functions for each terminal type.

namespace pycall {
namespace detail {

template<typename from_, typename to_t, typename caster_ = pycall::implicit_caster, typename ...template_args>
using from_from_macro = from_;
template<typename from_, typename to_t, typename caster_ = pycall::implicit_caster, typename ...template_args>
using to_from_macro = to_t;
template<typename from_, typename to_t, typename caster_ = pycall::implicit_caster, typename ...template_args>
using caster_from_macro = caster_;
template<typename from_, typename to_t, typename caster_ = pycall::implicit_caster, typename ...template_args>
using template_args_from_macro = pycall::typelist<template_args...>;

template<typename to_t, typename caster_t, typename tl>
struct connection_return_triple {
  connection_return_triple(to_t to, caster_t c, tl t) : to(to), caster(c), tlist(t) { }
  connection_return_triple(to_t to) : to(to) { }
  connection_return_triple() = default;

  to_t to;
  caster_t caster;
  tl tlist;
};

// input and return type of the connection function // Can be simplified (remove unnecessary *_from_macro declaratives)
template<typename from_, typename to_t, typename caster_ = pycall::implicit_caster, typename ...template_args>
using connection_return_t = pycall::detail::connection_return_triple< pycall::type<pycall::detail::to_from_macro<from_, to_t, caster_, template_args...>>,
                                      pycall::detail::caster_from_macro<from_, to_t, caster_, template_args...>,
                                      pycall::detail::template_args_from_macro<from_, to_t, caster_, template_args...>>;
template<typename from_, typename to_t, typename caster_ = pycall::implicit_caster, typename ...template_args>
using connection_input_t = pycall::type< pycall::detail::from_from_macro<from_, to_t, caster_, template_args...> >;

} // end namespace detail
} // end namespace pycall



/*!
 * \brief can be used inside a class body (type-map) to define \a class_name as a terminal type.
 *
 * \param class_name is the desired terminal type
 * \param str is the string connected to the \a class_name and it can be retrieved by the pycall::type_string() function.
 */
#define PYCALL_TERMINAL(class_name, str) \
  static pycall::detail::connection_return_triple<pycall::detail::type_string_t, pycall::none, pycall::typelist<>> connection(pycall::type< class_name > t) { \
    return pycall::detail::connection_return_triple<pycall::detail::type_string_t, pycall::none, pycall::typelist<>>({str}); \
  }

/*!
 * \brief connects type \a source_t (argument 1) to type \a dest_t (argument 2) using a custom caster \a caster_t (argument 3 and optional).
 * \a caster_t is a class with one or two public static member functions (upcast and downcast).
 * This macro takes two or three arguments where each argument must be a valid C++ type.
 * The first type can be a template type and is allowed to have commas in its template parameter list.
 *
 * If \a caster_t has just one of the two cast-functions, then the other cast must be possible with
 * a static_cast. The cast-functions have the following form:
 *
 * \code{.cpp}
 * static void caster_t::upcast(source_t from, dest_t &to);
 * \endcode
 * \code{.cpp}
 * static void caster_t::downcast(dest_t from, source_t &to);
 * \endcode
 *
 * Both cast-functions also support move-semantics on the first argument. The second argument must be a non-const reference
 * which is used for writing the return-value to.
 */
#define PYCALL_CONNECT(...) \
  static pycall::detail::connection_return_t<__VA_ARGS__> connection(pycall::detail::connection_input_t<__VA_ARGS__>) { \
    return pycall::detail::connection_return_t<__VA_ARGS__>(); \
  }

#endif // PYCALL_TRAITS_HPP
