#ifndef BINDING_DEFINITIONS_H
#define BINDING_DEFINITIONS_H

// Names of the dictionaries containing the overloads for the the stream write and read functions
// They map type_strings to corresponding functions.
#define STREAM_READ_DICT "stream_read_dict"
#define STREAM_WRITE_DICT "stream_write_dict"

#define CMD_STOP "stop"
#define CMD_CALL "call"
#define CMD_RETURN "return"
#define CMD_RETURN_VOID "ret_v"
#define CMD_ERROR "error"

#define VOID_TYPE_STRING "void_ts"

#endif // BINDING_DEFINITIONS_H
