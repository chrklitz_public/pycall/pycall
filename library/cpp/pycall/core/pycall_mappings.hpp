#ifndef PYCALL_MAPPINGS_HPP
#define PYCALL_MAPPINGS_HPP

#include <inttypes.h>
#include <string>
#include <vector>
#include <map>
#include <list>
#include <memory>

#include "pycall_traits.hpp"
#include "pycall_sequence_container_wrapper.hpp"
#include "pycall_tuple_wrapper.hpp"


namespace pycall {

struct map_caster
{

};
#define COMMA ,

/*!
 * \brief The default_mapping struct contains support for the most common types from the c++ standard template library.
 */
template<typename DerivedMapping>
struct abstract_default_mapping
{
  PYCALL_CONNECT(int8_t, int64_t)
  PYCALL_CONNECT(int16_t, int64_t)
  PYCALL_CONNECT(int32_t, int64_t)
  PYCALL_CONNECT(uint8_t, int64_t)
  PYCALL_CONNECT(uint16_t, int64_t)
  PYCALL_CONNECT(uint32_t, int64_t)
  PYCALL_CONNECT(uint64_t, int64_t)
  PYCALL_TERMINAL(int64_t, "int") // <-

  PYCALL_TERMINAL(std::string, "str") // <-

  PYCALL_CONNECT(float, double)
  PYCALL_TERMINAL(double, "double") // <-

  template<typename T>
  PYCALL_CONNECT(std::list<T>, std::shared_ptr<pycall::sequence_container_wrapper>, pycall::sequence_container_wrapper_caster<DerivedMapping>, T)
  template<typename T>
  PYCALL_CONNECT(std::vector<T>, std::shared_ptr<pycall::sequence_container_wrapper>, pycall::sequence_container_wrapper_caster<DerivedMapping>, T)

  template<typename... Args>
  PYCALL_CONNECT(std::tuple<Args...>, std::shared_ptr<pycall::tuple_wrapper>, pycall::tuple_wrapper_caster<DerivedMapping>, Args...)

  // TODO: Refactor PYCALL_CONNECT and PYCALL_TERMINAL syntax using a keyword-argument like syntax
  // TODO: Add support for templated terminal types
  template<typename Key, typename Val>
  PYCALL_TERMINAL(std::map<Key COMMA Val>, "dict")

  PYCALL_TERMINAL(std::shared_ptr<pycall::sequence_container_wrapper>, "list") // <-
  PYCALL_TERMINAL(std::shared_ptr<pycall::tuple_wrapper>, "tuple")
};

class default_mapping;
class default_mapping : public abstract_default_mapping<default_mapping> { };

} // end namespace pycall


#endif // PYCALL_MAPPINGS_HPP
