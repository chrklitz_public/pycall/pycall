/** \file pycaller.hpp
 * \brief Provides the pycall::pycaller class for making remote procedure calls on python functions.
 */

#ifndef PYCALLER_H
#define PYCALLER_H

#include "pycall.hpp"
#include <memory>

#include <iostream>
#include "binding_definitions.h" // TODO move to cpp file


namespace pycall {

/*!
 * \brief The pycaller class the C++ side api for calling python functions in another process in remote.
 */
template<typename Mapping, typename Stream>
class pycaller
{
  static_assert(std::is_base_of<pycall::stream, Stream>::value, "Stream type must inherit from pycall::stream");

private:
  pycaller(std::shared_ptr<Stream> stream)
    : stream(stream)
  {

  }

  // Extra function to visualize communication meta-data/overhead
  inline void stream_write_string(const std::string &s) {
    *stream << s;
  }

  // Iterates variadic template arguments by also considering the difference between rvalues and lvalues
  /*
  template<typename T, typename ...Ts>
  void add_arguments(T &arg, Ts&&... args) {
    add_single_argument(arg);
    add_arguments(std::forward<Ts>(args)...);
  }
  */
  template<typename T, typename ...Ts>
  void add_arguments(T &&arg, Ts&&... args) {
    stream_write_string(pycall::type_string<Mapping, T>());
    *stream << pycall::build_typestring_tree<Mapping, T>();
    *stream << pycall::cast_to_terminal<Mapping, T>(std::forward<T>(arg));
    add_arguments(std::forward<Ts>(args)...);
  }
  void add_arguments() { }

  // Get called for each argument
  /*
  template<typename T>
  void add_single_argument(T &arg) {
    stream_write_string(pycall::type_string<Mapping, T>());
    *stream << pycall::cast_to_terminal<Mapping, T>(arg);
  }
  */

  template<typename T>
  std::string type_string_wrapper(pycall::type<T>) {
    return pycall::type_string<Mapping, T>();
  }
  std::string type_string_wrapper(pycall::type<pycall::none>) {
    return VOID_TYPE_STRING;
  }

  void mark_error(bool &success) {
    std::string error_msg;
    *stream >> error_msg;
    std::cout << "pyreceiver::listen error: \'" << error_msg << "\'" << std::endl;
    error_occured = true;
    success = false;
  }

  template<typename Ret>
  void read_result_impl(const std::string &return_cmd, std::pair<Ret, bool> &res) {
    // makes sure that ret is initialized correctly -> a valid value
    // This is needed for values like shared pointers used for templated connections.
    // If ret is of type shared_ptr and not correctly initilized with a derived class
    // implementation, then the stream read call will fail because of the missing
    // dynamically binded stream read function.
    auto ret = pycall::cast_to_terminal<Mapping>(res.first);
    if (return_cmd == CMD_RETURN) {
      *stream >> ret; // read function return
    } else if (return_cmd == CMD_RETURN_VOID) {
      mark_error(res.second); // because Ret==pycall::none for non-void return type
    }
    res.first = pycall::cast_from_terminal<Mapping, Ret>(ret);
  }
  void read_result_impl(const std::string &return_cmd, std::pair<pycall::none, bool> &res) {
    if (return_cmd == CMD_RETURN) {
      mark_error(res.second); // because Ret==pycall::none for void return type
    } else if (return_cmd == CMD_RETURN_VOID) {
      // nothing to do here
    }
  }



public:
  /*!
   * \brief from_stream creates an instance of the pycaller class using the specified Mapping and an
   * interprocess stream.
   *
   * \param a arguments for the inteprocess stream
   * \return a new instance of this class
   */
  template<typename... Args>
  static pycaller from_stream(Args&& ...a)
  {
    return pycaller(std::shared_ptr<Stream>(new Stream(std::forward<Args>(a)...)));
  }

  /*!
   * \brief stops the listen-loop on the python-counterpart if not already done.
   */
  ~pycaller() {
    if (!stopped_event_loop) {
      stream_write_string(std::string(CMD_STOP));
      stream->send();
      stream->receive(); // make sure python process also has time to read data
    }
  }

  /*!
   * \brief key calls the pycall::stream::key() on the used stream instance.
   * \return the stream key
   */
  std::string key()
  {
    return stream->key();
  }

  /*!
   * \brief stop_listener manually stops the listen-loop in the python process. For now this also deactivates
   * this instance and makes it unuasable for more call() calls.
   */
  void stop_listener() {
    stream_write_string(CMD_STOP);
    stopped_event_loop = true;
    stream->send();
    stream->receive(); // block until listener received the stop signal
  }

  /*!
   * \brief call calls the specified python function \a fname .
   * \param fname identifies the python function by a dot separated list of module names followed by the function name.
   *
   * Regex: [<module-name>.]*(<function-name>) <br>
   * Examples: "numpy.zeros", "print", "numpy.linalg.norm"
   *
   * \param args is a parameter pack which takes any amount of arguments in any type supported in the Mapping class.
   *
   * Each of the arguments must be cast-able to a terminal type using pycall::cast_to_terminal() and a pycall
   * stream-write overload must exist for every terminal type.
   *
   * \return can take any type Ret which is supported in the Mapping class. The terminal type
   * of Ret must exist using pycall::terminal_type and a value of this terminal type must be cast-able
   * down to type Ret using pycall::cast_from_terminal() . Futhermore there must be a stream-read overload
   * for the terminal type.
   */
  template<typename Ret, typename ...Args>
  std::pair<Ret, bool> call(const std::string &fname, Args&&... args) {
    static_assert(!std::is_same<void, Ret>::value, "void is an invalid template type; Use pycall::none instead");

    // terminal type of the return-type
    using RetTermType = typename pycall::terminal_type<Mapping, Ret>::type; // none for Ret==none

    std::pair<Ret, bool> return_value;
    return_value.second = true;

    error_occured = false; // reset error information
    if (stopped_event_loop) {
      return_value.second = false;
      return return_value;
    }

    // command telling python what to do
    stream_write_string(std::string(CMD_CALL));

    // Add argument count to the stream
    stream_write_string(std::to_string(sizeof...(args)));

    // Add function name to the stream
    stream_write_string(fname);

    // Add the return-type type_string to the stream
    stream_write_string(pycall::type_string<Mapping, RetTermType>());
    *stream << pycall::build_typestring_tree<Mapping, Ret>(); // necessary for types like std::vector<T>

    // Add typestrings and arguments to the stream
    add_arguments(std::forward<Args>(args)...);

    // send data and receive result
    stream->send();

    // receive return data
    stream->receive();

    std::string return_cmd;
    *stream >> return_cmd;

    if (return_cmd == CMD_ERROR) {
      mark_error(return_value.second);
    } else {
      read_result_impl(return_cmd, return_value);
    }


    return return_value;
  }

private:
  std::shared_ptr<Stream> stream;
  bool error_occured = false;
  bool stopped_event_loop = false;
};


}

#endif // PYCALLER_H
