/** \file pycall_caster_traits.hpp
 * \brief Contains all sorts of utility around casting between types in a pycall type-map.
 */

#ifndef PYCALL_CASTER_TRAITS_HPP
#define PYCALL_CASTER_TRAITS_HPP

#include "pycall_connection_traits.hpp"
#include <type_traits>

/*!
 * \brief The pycall namespace is a collection of classes and functions providing a framework for enabling python remote procedure calls.
 */
namespace pycall {
struct implicit_caster { };

// Can be used for determining the type of the caster used for a connected type T
/*!
 * \brief The caster_type determines the type of the caster for a non-terminal type T.
 * If T is connected using PYCALL_CONNECT(T, B) then this returns pycall::detail::implicit_caster .
 * If T is connected using PYCALL_CONNECT(T, B, CustomCaster) then typename pycall::caster_type::type is
 * of type CustomCaster.
 */
template<typename Mapping, typename T, typename T_ = typename std::remove_const<typename std::remove_reference<T>::type>::type>
struct caster_type
{
  using type = decltype(Mapping::connection(pycall::type<T_>()).caster);
};
}

// =========================================================
// ======== CHECK EXISTENCE OF CUSTOM DOWN FUNCTION ========
// =========================================================
namespace pycall {
namespace detail {

/*
// Checks for the existence of a static downcast function in the type T
template <typename Mapping, typename T, bool is_implicit>
class has_custom_downcast_impl
{
public:
  static constexpr bool value = false;
};
template <typename Mapping, typename T>
class has_custom_downcast_impl<Mapping, T, false>
{
private:
  typedef char YesType[1];
  typedef char NoType[2];

  using caster_t = typename caster_type<Mapping, T>::type;
  using next_t = typename pycall::connected_type<Mapping, T>::type;

  template <typename C>
  static YesType& test( decltype(&C:: downcast) ) ;
  template <typename C>
  static NoType& test(...);

public:
  static constexpr bool value = (sizeof(test<caster_t>(0)) == sizeof(YesType));
};
*/


template<typename Mapping, typename T, bool is_implicit>
struct has_custom_downcast_impl
{
private:
  using caster_t = typename caster_type<Mapping, T>::type;
  using next_t = typename pycall::connected_type<Mapping, T>::type;

  caster_t *caster_ptr;
  next_t *next_ptr;
  T *T_ptr;

public:
  template<typename C>
  static auto test(C* obj, next_t *from, T *to)
   -> std::pair<decltype(obj->downcast(*from, *to)) *, char[1]>
  {
      return std::pair<decltype(obj->downcast(*from, *to)) *, char[1]>();
  }
  template<typename C>
  static auto test(...)
   -> std::pair<int, char[2]>
  {
      return std::pair<int, char[2]>();
  }

  static constexpr bool value = (sizeof(test<caster_t>(
                                          caster_ptr, next_ptr, T_ptr).second) == sizeof(char[1]));
};
template<typename Mapping, typename T>
struct has_custom_downcast_impl<Mapping, T, true>
{
  static constexpr bool value = false;
};
} // end namespace detail


/*!
 * \brief The has_custom_downcast struct checks if the specified mapping contains a custom downcast-function
 * for type T.
 */
template<typename Mapping, typename T, typename T_ = typename std::remove_const<typename std::remove_reference<T>::type>::type>
struct has_custom_downcast // next_t -> T
{
public:
  static constexpr bool value = pycall::detail::has_custom_downcast_impl<
      Mapping,
      T_,
      std::is_same<pycall::implicit_caster, typename pycall::caster_type<Mapping, T_>::type>::value
  >::value;
};
} // end namespace pycall


// =========================================================
// ====== CHECK EXISTENCE OF CUSTOM UP-CAST FUNCTION =======
// =========================================================
namespace pycall {
namespace detail {

/* // This is an old version with issues regarding the differentiation of templated and non-templated casters
// Checks for the existence of a static upcast function in the type T
template <typename Mapping, typename T, bool is_implicit>
class has_custom_upcast_impl
{
public:
  static constexpr bool value = false;
};
template <typename Mapping, typename T>
class has_custom_upcast_impl<Mapping, T, false>
{
private:
  typedef char YesType[1];
  typedef char NoType[2];

  using caster_t = typename caster_type<Mapping, T>::type;

  template <typename C>
  static YesType& test_templated( decltype(&C:: template upcast<T>) ) ;

  template <typename C>
  static NoType& test_templated(...);

public:
  static constexpr bool value = (sizeof(test_templated<caster_t>(0)) == sizeof(YesType));
};

*/

template<typename Mapping, typename T, bool is_implicit>
struct has_custom_upcast_impl
{
private:
  using caster_t = typename caster_type<Mapping, T>::type;
  using next_t = typename pycall::connected_type<Mapping, T>::type;

  caster_t *caster_ptr;
  T *T_ptr;
  next_t *next_ptr;

  static_assert(std::is_same<typename std::remove_const<next_t>::type, next_t>::value, "");
  static_assert(std::is_same<next_t &, decltype(*next_ptr)>::value, "");

public:
  template<typename C>
  static auto test(C* obj, T *from, next_t *to)
   -> std::pair<decltype(obj->upcast(*from, *to)) *, char[1]>
  {
      return std::pair<decltype(obj->upcast(*from, *to)) *, char[1]>();
  }
  template<typename C>
  static auto test(...)
   -> std::pair<int, char[2]>
  {
      return std::pair<int, char[2]>();
  }

  static constexpr bool value = (sizeof(test<caster_t>(
                                          caster_ptr, T_ptr, next_ptr).second) == sizeof(char[1]));
};
template<typename Mapping, typename T>
struct has_custom_upcast_impl<Mapping, T, true>
{
  static constexpr bool value = false;
};

/*
template<typename Mapping, typename T, typename caster_t, typename next_t>
auto has_custom_upcast_impl(caster_t* obj, T *from, next_t *to)
 -> std::pair<decltype(obj->upcast(*from, *to)) *, char[1]>
{
    return std::pair<decltype(obj->upcast(*from, *to)) *, char[1]>();
}
template<typename Mapping, typename T, typename caster_t, typename next_t>
auto has_custom_upcast_impl(...)
 -> std::pair<char, char[2]>
{
    return std::pair<char, char[2]>();
}
*/
} // end namespace detail

/*!
 * \brief The has_custom_upcast struct checks if the specified mapping contains a custom upcast-function
 * for type T.
 */
template<typename Mapping, typename Q>
struct has_custom_upcast
{
private:
  using T = typename std::remove_const<typename std::remove_reference<Q>::type>::type;
  using caster_t = typename caster_type<Mapping, T>::type;
  using next_t = typename connected_type<Mapping, T>::type;

public:
  static constexpr bool value = pycall::detail::has_custom_upcast_impl<
      Mapping,
      T,
      std::is_same<pycall::implicit_caster, caster_t>::value
  >::value;
};
} // end namespace pycall


// =========================================================
// ============= CAST UP/DOWN ONE CONNECTION ===============
// =========================================================
namespace pycall {

/*!
 * \brief upcast casts \a obj one step towards its terminal type. E.g. if type T is connected to type X, it will be casted
 * into type X. This overload casts \a obj using its custom upcast function specified in Mapping.
 *
 * \param obj which should be casted
 * \return the casted object
 */
template<typename Mapping,
         typename T,
         typename std::enable_if<has_custom_upcast<Mapping, T>::value, bool>::type = true,
         typename caster_t = typename caster_type<Mapping, T>::type,
         typename next_t = typename pycall::connected_type<Mapping, T>::type>
next_t upcast(T &&obj) {
  next_t casted;
  caster_t::upcast(std::forward<T>(obj), casted);
  return casted;
}
/*!
 * \brief upcast casts \a obj one step towards its terminal type. E.g. if type T is connected to type X, it will be casted
 * into type X. This overload casts \a obj using a static_cast.
 *
 * \param obj which should be casted
 * \return the casted object
 */
template<typename Mapping,
         typename T,
         typename std::enable_if<!has_custom_upcast<Mapping, T>::value, bool>::type = true,
         typename next_t = typename connected_type<Mapping, T>::type>
next_t upcast(T &&obj) {
  return static_cast<next_t>(std::forward<T>(obj));
}



/*!
 * \brief downcast casts \a of type Q to type T which is directly connected to type Q in the template type Mapping.
 * This overload casts \a obj using its custom downcast function specified in Mapping.
 *
 * \param obj which should be casted
 * \return the casted object
 */
template<typename Mapping,
         typename T,
         typename Q,
         typename Q_ = typename std::remove_const<typename std::remove_reference<Q>::type>::type,
         typename std::enable_if<has_custom_downcast<Mapping, T>::value, bool>::type = true,
         typename caster_t = typename caster_type<Mapping, T>::type
         >
T downcast(Q &&obj) { // -> T is connected to Q
  static_assert(std::is_same<Q_, typename pycall::connected_type<Mapping, T>::type>::value, "");
  T casted;
  caster_t::downcast(std::forward<Q>(obj), casted);
  return casted;
}



/*!
 * \brief downcast casts \a of type Q to type T which is directly connected to type Q in the template type Mapping.
 * This overload casts \a obj using a static_cast.
 *
 * \param obj which should be casted
 * \return the casted object
 */
template<typename Mapping,
         typename T,
         typename Q,
         typename Q_ = typename std::remove_const<typename std::remove_reference<Q>::type>::type,
         typename std::enable_if<!has_custom_downcast<Mapping, T>::value, bool>::type = true
         >
T downcast(Q &&obj) { // -> T is connected to Q
  static_assert(std::is_same<Q_, typename pycall::connected_type<Mapping, T>::type>::value, "");
  return static_cast<T>(std::forward<Q>(obj));
}



} // end namespace pycall


// =========================================================
// ============== DETERMINING TERMINAL TYPE ================
// =========================================================
namespace pycall {
namespace detail {
// The int N template parameter is used to generate unique types for every recursive type definition.
// Otherwise the "using type = ..." line would make use of its surrounding type which is incomplete
// at this point.
template<typename Mapping, typename T, int N, bool is_final>
struct type_res_terminal_type_impl
{
  using next_t = typename pycall::connected_type<Mapping, T>::type;
  using type = typename type_res_terminal_type_impl<Mapping, next_t, N+1, pycall::is_terminal<Mapping, next_t>::value>::type;
};

template<typename Mapping, typename T, int N>
struct type_res_terminal_type_impl<Mapping, T, N, true>
{
  using type = T;
};
} // end namespace detail


/*!
 * \brief The terminal_type struct determines the terminal type of the template type T by
 * recursively following the connections in the Mapping type. If template T is pycall::none, than
 * the resulting terminal-type is pycall::none too.
 */
template<typename Mapping, typename T, typename T_ = typename std::remove_const<typename std::remove_reference<T>::type>::type>
struct terminal_type
{
  using type = typename pycall::detail::type_res_terminal_type_impl<Mapping, T_, 0, pycall::is_terminal<Mapping, T_>::value>::type;
};
template<typename Mapping>
struct terminal_type<Mapping, pycall::none>
{
  using type = pycall::none;
};

} // end namespace pycall


// =========================================================
// ============= CAST TO/FROM TERMINAL TYPE ================
// =========================================================
namespace pycall {
// Casts the input object to its terminal type using implicit conversion where
// possible and an overloadable pycall function instead. // TODO: Add move-semantics

/*!
 * \brief cast_to_terminal is an overload which is activated when called with a terminal type T.
 * This overload just returns its input.
 *
 * \param obj of terminal type T
 * \return \a obj
 */
template<typename Mapping, typename T,
         typename std::enable_if<is_terminal<Mapping, T>::value, bool>::type = true>
inline T cast_to_terminal(const T &obj) {
  return obj;
}
/*!
 * \brief cast_to_terminal is an overload which is activated when called with a non-terminal type T. It
 * recursively casts \a obj to its terminal type and returns the result. Type T must be connected to
 * a terminal type inside the specified Mapping class. It uses ::upcast() for casting.
 *
 * \param obj of non-terminal type T
 * \return casted \a obj
 */
template<typename Mapping, typename T,
         typename std::enable_if<!is_terminal<Mapping, T>::value, bool>::type = true>
typename terminal_type<Mapping, T>::type cast_to_terminal(T &&obj) {
  // cast obj to next_t using implicit cast or overloadable pycall function.
  // continue casting along connection-path.
  return cast_to_terminal<Mapping>(pycall::upcast<Mapping, T>(std::forward<T>(obj)));
}




// Casts the input object (of a terminal type) to the specified type T.
// This function to work, requires T to be directly or indirectly connected to the type of obj.
/*!
 * \brief cast_from_terminal is an overload which is activated when called with a terminal type T.
 * This overload just returns its input.
 *
 * \param obj of terminal type T
 * \return \a obj
 */
template<typename Mapping, typename T,
         typename std::enable_if<is_terminal<Mapping, T>::value, bool>::type = true>
inline const T & cast_from_terminal(const T &obj) {
  return obj;
}
template<typename Mapping, typename T,
         typename std::enable_if<is_terminal<Mapping, T>::value, bool>::type = true>
inline T&& cast_from_terminal(T &&obj) {
  return std::move(obj);
}

/*!
 * \brief cast_from_terminal is an overload which is activated when called with a non-terminal type T.
 * It recursively casts \a obj up to type T. Type T must be connected to type F inside the specified Mapping class.
 * It uses ::downcast() for casting.
 *
 * \param obj of non-terminal type T
 * \return casted \a obj
 */
template<typename Mapping,
         typename T,
         typename F,
         typename std::enable_if<!is_terminal<Mapping, T>::value, bool>::type = true>
T cast_from_terminal(F &&obj) {
  using next_t = typename connected_type<Mapping, T>::type;

  // recursively cast up towards T
  // next_t next = cast_from_terminal<Mapping, next_t>(std::forward<F>(obj));

  // cast next to type T using implicit cast or overloadable pycall function
  return pycall::downcast<Mapping, T>(cast_from_terminal<Mapping, next_t>(std::forward<F>(obj)));
}
} // end namespace pycall





#endif // PYCALL_CASTER_TRAITS_HPP
