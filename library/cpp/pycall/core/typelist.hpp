#ifndef PYCALL_TYPELIST_HPP
#define PYCALL_TYPELIST_HPP

namespace pycall {

struct none { }; // used as a type replacement for void

template<typename...>
struct typelist { };

template<typename T, typename... Ts>
struct typelist<T, Ts...>
{
  constexpr static bool empty = false;
  using head = T;
  using tail = typelist<Ts...>;
};

template<typename T>
struct typelist<T>
{
  constexpr static bool empty = false;
  using head = T;
  using tail = typelist<>;
};

template<>
struct typelist<>
{
  constexpr static bool empty = true;
  using head = pycall::none;
  using tail = pycall::none;
};



} // end namespace pycall

#endif // PYCALL_TYPELIST_HPP
