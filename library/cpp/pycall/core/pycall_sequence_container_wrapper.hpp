#ifndef PYCALL_SEQUENCE_CONTAINER_WRAPPER_HPP
#define PYCALL_SEQUENCE_CONTAINER_WRAPPER_HPP

#include <vector>
#include <utility>
#include <memory>
#include "pycall_connection_traits.hpp"
#include "pycall_caster_traits.hpp"
#include "pycall_stream.hpp"

#include <iostream>


namespace pycall {

class stream;

class sequence_container_wrapper {
public:
  virtual void write_to_stream(pycall::stream &s) = 0;
  virtual void read_from_stream(pycall::stream &s) = 0;
  virtual ~sequence_container_wrapper() = default;
};

template<typename Mapping, typename T, typename TerminalType = typename pycall::terminal_type<Mapping, T>::type>
class sequence_container_wrapper_typed : public pycall::sequence_container_wrapper {
public:
  sequence_container_wrapper_typed(std::vector<TerminalType> &&list)
    : container(std::forward<std::vector<TerminalType>>(list))
  { }
  sequence_container_wrapper_typed(const std::list<TerminalType> &list) {
    container.reserve(list.size());
    for (const auto &obj : list) {
      container.push_back(obj);
    }
  }
  sequence_container_wrapper_typed(std::list<TerminalType> &&list) {
    container.reserve(list.size());
    for (const auto &obj : list) {
      container.emplace_back(std::move(obj));
    }
  }
  sequence_container_wrapper_typed() = default;

  sequence_container_wrapper_typed(sequence_container_wrapper_typed &&other)
  {
    *this = std::forward<sequence_container_wrapper_typed>(other);
  }

  sequence_container_wrapper_typed &operator=(const sequence_container_wrapper_typed &other) {
    if (this != &other) {
      container = other.container;
    }
  }
  sequence_container_wrapper_typed &operator=(sequence_container_wrapper_typed &&other) {
    if (this != &other) {
      container = std::move(other.container);
    }
  }

  ~sequence_container_wrapper_typed() override = default;

  operator std::vector<TerminalType>() {
    return container;
  }
  operator std::list<TerminalType>() {
    return std::list<TerminalType>(container.begin(), container.end());
  }

  virtual void write_to_stream(stream &s) override {
    s << static_cast<uint64_t>(container.size());
    for (const auto &obj : container) {
      s << obj;
    }
  }

  virtual void read_from_stream(stream &s) override {
    uint64_t size;
    s >> size;
    for (uint64_t i = 0; i < size; ++i) {
      // makes a valid object even if it is a shared pointer -> needs T to be default-constructible
      TerminalType obj = pycall::cast_to_terminal<Mapping>(T());
      s >> obj;
      container.emplace_back(std::move(obj));
    }
  }

  std::vector<TerminalType> container;
};





template<typename Mapping>
struct sequence_container_wrapper_caster
{
  template<typename Container, typename __help = decltype(Container().reserve(0))>
  static inline void reserve(size_t n, Container &container) {
    container.reserve(n);
  }

  template<typename Container>
  static inline void reserve(...) {
    // pass
  }

  template<typename Container, typename T = typename Container::value_type>
  static void upcast(const Container &from, std::shared_ptr<pycall::sequence_container_wrapper> &to) {
    std::cout << "upcast lvalue" << std::endl;
    pycall::sequence_container_wrapper_typed<Mapping, T> *temp = new pycall::sequence_container_wrapper_typed<Mapping, T>();

    temp->container.reserve(from.size());
    for (const auto &obj : from) {
      temp->container.push_back(pycall::cast_to_terminal<Mapping>(obj));
    }

    to = std::shared_ptr<pycall::sequence_container_wrapper>(temp);
  }
  template<typename Container, typename T = typename Container::value_type>
  static void upcast(Container &&from, std::shared_ptr<pycall::sequence_container_wrapper> &to) {
    std::cout << "upcast rvalue" << std::endl;
    pycall::sequence_container_wrapper_typed<Mapping, T> *temp = new pycall::sequence_container_wrapper_typed<Mapping, T>();

    temp->container.reserve(from.size());
    for (const auto &obj : from) {
      temp->container.emplace_back(pycall::cast_to_terminal<Mapping>(std::move(obj)));
    }

    to = std::shared_ptr<pycall::sequence_container_wrapper>(temp);
  }


  template<typename Container, typename T = typename Container::value_type>
  static void downcast(const std::shared_ptr<pycall::sequence_container_wrapper> &from, Container &to) {
    std::cout << "downcast lvalue" << std::endl;
    pycall::sequence_container_wrapper_typed<Mapping, T> &temp = dynamic_cast<pycall::sequence_container_wrapper_typed<Mapping, T> &>(*from);

    reserve<Container>(temp.container.size(), to);
    for (const auto &obj : temp.container) {
      to.push_back(pycall::cast_from_terminal<Mapping, T>(obj));
    }
  }
  template<typename Container, typename T = typename Container::value_type>
  static void downcast(std::shared_ptr<pycall::sequence_container_wrapper> &&from, Container &to) {
    std::cout << "downcast rvalue" << std::endl;
    using ttype = typename pycall::terminal_type<Mapping, T>::type;
    pycall::sequence_container_wrapper_typed<Mapping, T> &temp = dynamic_cast<pycall::sequence_container_wrapper_typed<Mapping, T> &>(*from);

    reserve<Container>(temp.container.size(), to);
    for (auto &obj : temp.container) {
      to.emplace_back(pycall::cast_from_terminal<Mapping, T>(std::move(obj)));
    }
  }
};


} // end namespace pycall

inline pycall::stream &operator<<(pycall::stream &s, std::shared_ptr<pycall::sequence_container_wrapper> container) {
  container->write_to_stream(s);
  return s;
}
inline pycall::stream &operator>>(pycall::stream &s, std::shared_ptr<pycall::sequence_container_wrapper> &container) {
  container->read_from_stream(s);
  return s;
}


#endif // PYCALL_SEQUENCE_CONTAINER_WRAPPER_HPP
