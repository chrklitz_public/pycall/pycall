/** \file pycall_connection_traits.hpp
 * \brief Contains all sorts of utility around type-connections.
 */

#ifndef PYCALL_CONNECTION_TRAITS_HPP
#define PYCALL_CONNECTION_TRAITS_HPP

#include "binding_definitions.h"
#include "typestring_tree.hpp"
#include <type_traits>
#include "typelist.hpp"

#include <iostream>

// =========================================================
// ============= GENERAL TYPE CONNECTION UTIL ==============
// =========================================================
namespace pycall {
/*!
 * \brief The type struct is a lightweight way of carrying type information to function-overloads
 * to prevent the problem with partial specialization.
 */
template<typename T>
struct type { };

namespace detail {

// Wrapper type for the type_string. Necessary for template-type-deduction
struct type_string_t
{
  using type = std::string; // necessary for PYCALL_TERMINAL macro
  std::string type_string;
};

// Recursively determines the type_string of a type
// by following its connections to its terminal type
template<typename Mapping, typename T = void>
std::string get_type_string_impl(type_string_t t) {
  return t.type_string;
}
template<typename Mapping, typename T>
std::string get_type_string_impl(pycall::type<T> t) {
  using result_t = decltype(Mapping::connection(t).to);
  return get_type_string_impl<Mapping>(Mapping::connection(t).to);
}

template<typename Mapping>
std::string get_type_string_impl(pycall::type<pycall::none>) {
  return VOID_TYPE_STRING;
}



// Extract the template type Q of T=type<Q>.
// The Mapping is used to prevent template extraction of terminal types.
// This is used by the pycall::is_terminal type-alias
template<typename Mapping, typename T>
struct type_template_type
{
  static_assert(!std::is_same<type_string_t, T>::value, "typename T must be a non-terminal type");
  using type = T;
};
template<typename Mapping, template<typename> class T, typename Q>
struct type_template_type<Mapping, T<Q>>
{
  using type = Q;
};
} // end namespace detail


/*!
 * \brief is_terminal determines if a type T is a terminal type in the specified Mapping class.
 * Use is_terminal<Mapping, T>::value to get the bool value.
 */
template<typename Mapping, typename T, typename T_ = typename std::remove_const<typename std::remove_reference<T>::type>::type>
using is_terminal = std::is_same<decltype(Mapping::connection(pycall::type<T_>()).to), detail::type_string_t>;


// determines the to T connected type if T is non-terminal.
/*!
 * \brief connected_type determines the type where T is connected to. T is not allowed to
 * be a terminal-type in the Mapping class.
 */
template<typename Mapping, typename T, typename T_ = typename std::remove_const<typename std::remove_reference<T>::type>::type>
using connected_type = typename detail::type_template_type<Mapping, decltype(Mapping::connection(pycall::type<T_>()).to)>;

template<typename Mapping, typename T, typename T_ = typename std::remove_const<typename std::remove_reference<T>::type>::type>
using template_types_of = decltype(Mapping::connection(pycall::type<T_>()).tlist);

// Recursively determines the type_string of the transitively connected terminal type
// using the specified Mapping. If T is itself transitive,
// then its connected type_string is returned directly.
/*!
 * \brief type_string recursively determines the type-string of the transitively connected terminal type
 * using the specified type-map Mapping.
 *
 * \return the type-string of the terminal type of T
 */
template<typename Mapping, typename T>
std::string type_string() {
  return detail::get_type_string_impl<Mapping>(pycall::type<T>());
}

namespace detail {

template<typename Mapping, typename tl, typename std::enable_if<tl::empty, bool>::type = true>
void build_typestring_tree_impl(typestring_tree &) { }

template<typename Mapping, typename tl, typename std::enable_if<!tl::empty, bool>::type = true>
void build_typestring_tree_impl(typestring_tree &t) {
  using head = typename tl::head;
  using tail = typename tl::tail;

  typestring_tree &child = t.push_back(type_string<Mapping, head>());
  build_typestring_tree_impl<Mapping, tail>(t);
  build_typestring_tree_impl<Mapping, template_types_of<Mapping, head>>(child);
}

// Wrapping this implementation in a class allows
// partial specialization to differ on T=pycall::none.
template<typename Mapping, typename T>
struct build_typestring_tree_help
{
  static typestring_tree build() {
    using tl = template_types_of<Mapping, T>;
    typestring_tree root;
    pycall::detail::build_typestring_tree_impl<Mapping, tl>(root);
    return root;
  }
};
template<typename Mapping>
struct build_typestring_tree_help<Mapping, pycall::none>
{
  static typestring_tree build() {
    return typestring_tree();
  }
};

} // end namespace detail


template<typename Mapping, typename T>
typestring_tree build_typestring_tree() {
  return pycall::detail::build_typestring_tree_help<Mapping, T>::build();
}


} // end namespace pycall


#endif // PYCALL_CONNECTION_TRAITS_HPP
