#include "interprocess_messaging.h"
#include <QSharedMemory>
#include <QSystemSemaphore>
#include <cstring>
#include <QCoreApplication>
#include <random>
#include <chrono>

#include <iostream>


char *increment_ptr(char **buffer, int n) {
  char *ptr = *buffer;
  *buffer += n;
  return ptr;
}

int interprocess_messaging::instance_cnt = 0;

#define NUM_KEYS_SIZE (sizeof(interprocess_messaging::hsize_t))
#define IS_FINAL_SIZE 1

#define HEADER_SIZE (NUM_KEYS_SIZE + IS_FINAL_SIZE)


interprocess_messaging::comm_channel::comm_channel(char *bytes, hsize_t size)
  : num_strings(reinterpret_cast<hsize_t *>(increment_ptr(&bytes, NUM_KEYS_SIZE))),
    is_final(reinterpret_cast<bool *>(increment_ptr(&bytes, IS_FINAL_SIZE))),
    bytes(static_cast<char *>(bytes)),
    bytessize(size - HEADER_SIZE)
{ }


void interprocess_messaging::comm_channel::flush()
{
  *num_strings = 0;
  *is_final = false;
  this->pos = 0;
}

void interprocess_messaging::comm_channel::finish()
{
  *is_final = true;
}

bool interprocess_messaging::comm_channel::try_add_string(const std::string &str)
{
  // index pointing to the null-character at the end of the string
  hsize_t end_index = pos + static_cast<hsize_t>(str.size());

  if (end_index >= bytessize)
    return false;

  std::memcpy(bytes + pos, str.c_str(), str.size() + 1);
  *num_strings += 1;
  pos = end_index + 1;

  return true;
}

bool interprocess_messaging::comm_channel::read_available_strings(std::vector<std::string> &vec)
{
  hsize_t n = *num_strings;
  vec.reserve(n);
  hsize_t str_index = 0;
  for (hsize_t i = 0; i < n; ++i) {
    vec.push_back(bytes + str_index);
    str_index += vec.back().size() + 1;
  }
  return *is_final;
}

std::string interprocess_messaging::systemunique_key()
{
  std::srand(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count());

  QString key = "ipm"
      + QString::number(std::rand())
      + "_"
      + QString::number(QCoreApplication::applicationPid())
      + "_"
      + QString::number(interprocess_messaging::instance_cnt);
  return key.toStdString();
}

interprocess_messaging::interprocess_messaging(int buffer_size)
{
  using mptr = std::shared_ptr<QSharedMemory>;
  using sptr = std::shared_ptr<QSystemSemaphore>;

  QString key = QString::fromStdString(systemunique_key());

  comm_buffer = mptr(new QSharedMemory(key));
  this_proc_sem = sptr(new QSystemSemaphore(key + "sem1", 0));
  other_proc_sem = sptr(new QSystemSemaphore(key + "sem2", 0));

  if (!comm_buffer->create(buffer_size + HEADER_SIZE, QSharedMemory::ReadWrite)) {
    set_error(comm_buffer->errorString().toStdString());
    return;
  }

  comm = comm_channel(static_cast<char *>(comm_buffer->data()), comm_buffer->size());
}

interprocess_messaging::interprocess_messaging(const std::string &existing_key)
{
  using mptr = std::shared_ptr<QSharedMemory>;
  using sptr = std::shared_ptr<QSystemSemaphore>;

  QString key = QString::fromStdString(existing_key);

  comm_buffer = mptr(new QSharedMemory(key));
  this_proc_sem = sptr(new QSystemSemaphore(key + "sem2", 0));
  other_proc_sem = sptr(new QSystemSemaphore(key + "sem1", 0));

  if (!comm_buffer->attach()) {
    set_error(comm_buffer->errorString().toStdString());
    return;
  }

  comm = comm_channel(static_cast<char *>(comm_buffer->data()), comm_buffer->size());
}

std::string interprocess_messaging::key() const
{
  return comm_buffer->key().toStdString();
}

bool interprocess_messaging::error()
{
  return error_occured;
}

std::string interprocess_messaging::error_string()
{
  return error_description;
}

void interprocess_messaging::add_string(const std::string &s)
{
  if (error())
    return;

  send_buffer.push_back(s);
}

const std::vector<std::string> &interprocess_messaging::received_strings() const
{
  return receive_buffer;
}

#define RELEASE_SEM(sem) \
  if (!sem->release()) { \
    set_error(sem->errorString().toStdString()); \
    return false; \
  }

#define ACQUIRE_SEM(sem) \
  if (!sem->acquire()) { \
    set_error(sem->errorString().toStdString()); \
    return false; \
  }



bool interprocess_messaging::send()
{
  if (error())
    return false;

  comm.flush();

  for (auto &s : send_buffer) {
    if (!this->comm.try_add_string(s)) {
      // barrier synchronization with receive; see (LOOP_BARRIER)
      // tells the other process that data is ready and must be read.
      RELEASE_SEM(other_proc_sem)
      ACQUIRE_SEM(this_proc_sem)
      // other process finished reading available data

      comm.flush();
      // must work second time due to empty comm channel
      comm.try_add_string(s);
    }
  }

  // semaphore state same as before of the function
  comm.finish();
  send_buffer.clear(); // all strings got sent

  // signal that data is ready and ensure data gets received
  RELEASE_SEM(other_proc_sem)
  ACQUIRE_SEM(this_proc_sem)

  return true;
}

bool interprocess_messaging::receive()
{
  if (error())
    return false;

  receive_buffer.clear();

  ACQUIRE_SEM(this_proc_sem) // ensure data is ready

  while (true) {
    if (comm.read_available_strings(receive_buffer))
      break; // true if this read the final send

    // barrier synchronization with send; see (LOOP_BARRIER)
    // -> wait for more data
    RELEASE_SEM(other_proc_sem)
    ACQUIRE_SEM(this_proc_sem)
  }

  // signal other process that data got received
  RELEASE_SEM(other_proc_sem)

  return true;
}

void interprocess_messaging::set_error(const std::string &description)
{
  this->error_description = description;
  error_occured = true;

  // detach shared memory
  if (!comm_buffer->isAttached())
    comm_buffer->detach();

  // deallocate memory
  send_buffer.resize(0);
  receive_buffer.resize(0);
}

