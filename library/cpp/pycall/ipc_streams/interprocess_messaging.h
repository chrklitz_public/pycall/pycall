/**
 * \file interprocess_messaging.h
 */

#ifndef INTERPROCESS_MESSAGING_H
#define INTERPROCESS_MESSAGING_H


#include <vector>
#include <inttypes.h>
#include <memory>

class QSharedMemory;
class QSystemSemaphore;
class QString;

/*!
 * \brief The interprocess_messaging class allows interprocess communication of strings using shared memory
 * and system semaphores. It is more primitive than the interprocess_stream class because it only allows communication via strings
 * and not raw data.
 */
class interprocess_messaging
{
  using hsize_t = uint32_t;

  static int instance_cnt;
  static std::string systemunique_key();


  class comm_channel
  {
  public:
    comm_channel(char *bytes, hsize_t size);
    comm_channel() = default;

    void flush();
    void finish();

    // return true on success
    // returns false if buffer is full (str won't be added then)
    bool try_add_string(const std::string &str);

    // return true only if it is the final batch (*is_final==true)
    // Adds the available strings into the given vector vec
    bool read_available_strings(std::vector<std::string> &vec);

  private:
    // HEADER
    hsize_t *num_strings; // holds the number of strings send via the comm channel
    bool *is_final; // marks if all strings got sent or if there are more

    // DATA
    char *bytes;

    // size of the bytes array
    hsize_t bytessize;

    // startindex in bytes for the next string
    hsize_t pos = 0;
  };


public:
  interprocess_messaging(int buffer_size); // creates the comm object
  interprocess_messaging(const std::string &existing_key); // attaches to an existing comm object

  std::string key() const;
  bool error();
  std::string error_string();

  void add_string(const std::string &s);
  const std::vector<std::string> &received_strings() const;

  bool send(); // waits for corresponding receive
  bool receive(); // waits for corresponding send

private:
  void set_error(const std::string &description);

private:
  std::shared_ptr<QSharedMemory> comm_buffer;
  comm_channel comm;

  std::shared_ptr<QSystemSemaphore> this_proc_sem;
  std::shared_ptr<QSystemSemaphore> other_proc_sem;

  std::vector<std::string> receive_buffer;
  std::vector<std::string> send_buffer;

  std::string error_description;
  bool error_occured = false;
};



#endif // INTERPROCESS_MESSAGING_H
