/**
 * \file interprocess_stream.h
 */

#ifndef INTERPROCESS_STREAM_H
#define INTERPROCESS_STREAM_H

#include "interprocess_messaging.h"
#include <vector>
#include <type_traits>
#include <pycall/core/pycall_stream.hpp>

#define IPS_MIN_SEGMENT_SIZE 120 // + HEADER_SIZE < 128
#define IPS_MIN_COMM_CHANNEL_SIZE 120

// shared memory based interprocess communication
// This class dynamically allocates more shared memory (rows) if needed.
/*!
 * \brief The interprocess_stream class implements interprocess communication by making use of shared memory
 * and system semaphores. Furthermore it implements the pycall::stream interface which makes it compatible to
 * the pycall::pycaller / pyreceiver api.
 */
class interprocess_stream : public pycall::stream
{
public:
  using tagsize_t = uint32_t;
public:
  interprocess_stream();
  interprocess_stream(const std::string &key);

  std::string key() const override;

  // Those two functions make use of an extra size tag which tracks the size of the data written.
  // This releases the user from programming this explicitly.
  // When reading, size is set by the read-function.
  bool read_raw(void **data, size_t *size) override;
  bool add_raw(const void *data, size_t size) override;

  // Add/read data without an extra size tag before the actual data.
  // This means that read-calls must know the size of the data.
  bool read_raw_unchecked(void **data, size_t size);
  bool add_raw_unchecked(const void *data, size_t size);

  // Make sure that the last send call can take effect by not destroying the object until
  // the other process read everything!
  bool send() override;
  bool receive() override;

  void clear_send_buffer() override;

private:
  std::string unique_row_key();
  bool new_row(int data_size);
  void next_row();
  void clear();

private:
  interprocess_messaging mp; // messagepassing

  std::vector<std::shared_ptr<QSharedMemory>> rows;
  int row_index = 0; // stream pos index in the array of QSharedMemory segments
  int col_index = 0; // stream pos inside QSharedMemory::data

  bool send_mode = true;

  static uint64_t key_cnt; // used for creating unique keys per instance (system wide unique-ness is guaranteed in the key generation)
};



#endif // INTERPROCESS_STREAM_H
