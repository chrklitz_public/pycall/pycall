cmake_minimum_required(VERSION 3.13)

# ============ Qt ===============
find_package(Qt5 COMPONENTS Core REQUIRED)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)
if(CMAKE_VERSION VERSION_LESS "3.7.0")
    set(CMAKE_INCLUDE_CURRENT_DIR ON)
endif()

# ============ PROJECT ==========

set(HEADERS
    interprocess_messaging.h
    interprocess_stream.h
)
set(SOURCES
    interprocess_messaging.cpp
    interprocess_stream.cpp
)
set(FILES
    ${HEADERS}
    ${SOURCES}
)

add_library(ipc_streams SHARED ${FILES})
target_include_directories(ipc_streams PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/../..>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
)

# ========= LINK STUFF =========
target_link_libraries(ipc_streams PUBLIC Qt5::Core core)

if (USE_PYCALL_CPP)
    # install headers
    install(FILES ${HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/pycall/ipc_streams)

    install(TARGETS ipc_streams
            EXPORT pycallTargets
            LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
            ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
            RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
            INCLUDES DESTINATIO$N ${CMAKE_INSTALL_INCLUDEDIR}
    )
endif()




