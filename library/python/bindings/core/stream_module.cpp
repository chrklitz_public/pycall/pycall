#include "stream_module.h"

stream_module::stream_module()
{

}

stream_module::stream_module(const stream_module::function_map &funcs)
  : funcs(funcs)
{ }

void stream_module::import_stream_module(stream_module &other, bool fail_on_collision)
{
  for (const auto &p : other.funcs) {
    if (funcs.find(p.first) != funcs.end()) {
      if (fail_on_collision)
        throw "Key already in stream module";
    }
    funcs[p.first] = p.second;
  }
}

stream_module::stream_read_func *stream_module::find_stream_read(const std::string &typestring)
{
  if (funcs.find(typestring) != funcs.end())
    return &funcs[typestring].first;
  else
    return nullptr;
}

stream_module::stream_write_func *stream_module::find_stream_write(const std::string &typestring)
{
  if (funcs.find(typestring) != funcs.end())
    return &funcs[typestring].second;
  else
    return nullptr;
}

void bind_stream_module(pybind11::module &m)
{
  py::class_<stream_module>(m, "stream_module")
      .def(py::init<>())
      .def(py::init<const stream_module::function_map&>())
      .def("import_stream_module", &stream_module::import_stream_module)
      .def("find_stream_read", &stream_module::find_stream_read)
      .def("find_stream_write", &stream_module::find_stream_write)
      ;
}
