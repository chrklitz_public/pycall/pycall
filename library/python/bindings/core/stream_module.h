#ifndef STREAM_MODULE_H
#define STREAM_MODULE_H

#include <pybind11/pybind11.h>

#include <map>
#include <list>
#include <functional>
#include <string>
#include <pycall/core/pycall_stream.hpp>
#include <pycall/core/pycall_mappings.hpp>
#include <pycall/core/typestring_tree.hpp>

namespace py = pybind11;

void bind_stream_module(pybind11::module &m);

class stream_module
{
public:
  using stream_read_func = std::function<py::object(pycall::stream &, stream_module &, const pycall::typestring_tree &)>;
  using stream_write_func = std::function<void(pycall::stream &, py::object, stream_module &, const pycall::typestring_tree &)>;
  using function_map = std::map<std::string, std::pair<stream_read_func, stream_write_func>>;


  stream_module();
  stream_module(const function_map &funcs);

  /*!
   * \brief import_stream_module imports another stream module and adds its functions to this one.
   * \param other which should be imported
   * \param fail_on_collision determines if this function throws an error if a typestring collision occures.
   * If this is set to false, then the colliding stream functions from the new module replace the functions
   * from this module.
   */
  void import_stream_module(stream_module &other, bool fail_on_collision=true);
  stream_read_func *find_stream_read(const std::string &typestring);
  stream_write_func *find_stream_write(const std::string &typestring);

private:
  function_map funcs;
};

#endif // STREAM_MODULE_H
