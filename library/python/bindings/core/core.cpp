#include <pybind11/pybind11.h>
#include <pybind11/buffer_info.h>
namespace py = pybind11;

#include "pyreceiver.h"
#include "default_comm.h"


PYBIND11_MODULE(_core, m) {
  bind_pyreceiver(m);
  bind_stream_module(m);

  py::class_<modules> mods(m, "modules");

  mods.def_readonly_static("default_comm",  &modules::default_comm);

  // Make the pyreceiver class and the pycaller class a lot more stable by adding lots of checks
  // to prevent infinite blocking or segfaults due to synchronization issues.
  // Most important things are stability regarding:
  //
  // invalid pycaller.call arguments, python function not found, invalid python return argument (type, count, ...),
  // python function exception, invalid argument en-/decoding from/to stream.

  //
}
