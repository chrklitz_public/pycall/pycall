#include <pybind11/pybind11.h>
namespace py = pybind11;

#include "default_comm.h"
#include "stream_module.h"


#define ID(x) x // use this instead of py::int_ or similar things if python-to-python type-casting is not wanted
#define TYPE_MAP(FUNC) FUNC(int64_t, py::int_), FUNC(std::string, py::str), FUNC(double, py::float_)

#define READ_WRITE(t, CAST) \
{ \
  pycall::type_string<pycall::default_mapping, t>(), \
  { \
    [](pycall::stream &stream, stream_module &, const pycall::typestring_tree &) { \
      t obj; \
      stream >> obj; \
      return py::cast(obj); \
    }, \
    [](pycall::stream &stream, py::object obj, stream_module &, const pycall::typestring_tree &) { \
      stream << CAST(obj).cast<t>(); \
    } \
  } \
}

static stream_module::function_map default_function_map =
{
  TYPE_MAP(READ_WRITE),

  {
    pycall::type_string<pycall::default_mapping, std::shared_ptr<pycall::sequence_container_wrapper>>(),
    {
      [](pycall::stream &stream, stream_module &m, const pycall::typestring_tree &metadata) {
        const auto entries_ts = metadata.entries();
        if (entries_ts.size() != 1)
          throw std::invalid_argument("list stream-read expects epects exactly one typestring for describing its entries type");

        const std::string &entry_ts = entries_ts[0].first;
        const pycall::typestring_tree &entry_metadata = entries_ts[0].second;

        stream_module::stream_read_func *read_element = m.find_stream_read(entry_ts);
        if (read_element == nullptr)
          throw std::invalid_argument("Missing stream-read function for typestring '" + entry_ts + "'");

        // Read obj from stream
        uint64_t length;
        stream >> length;
        py::list l;
        for (size_t i = 0; i < static_cast<size_t>(length); ++i) {
          l.append((*read_element)(stream, m, entry_metadata));
        }
        return l;
      },
      [](pycall::stream &stream, py::object obj, stream_module &m, const pycall::typestring_tree &metadata) {
        const auto entries_ts = metadata.entries();
        if (entries_ts.size() != 1)
          throw std::invalid_argument("list stream-write expects exactly one typestring for describing its entries type");

        const std::string &entry_ts = entries_ts[0].first;
        const pycall::typestring_tree &entry_metadata = entries_ts[0].second;

        stream_module::stream_write_func *write_element = m.find_stream_write(entry_ts);
        if (write_element == nullptr)
          throw std::invalid_argument("Missing stream-write function for typestring '" + entry_ts + "'");

        // Write obj to stream
        py::list l = obj;
        stream << static_cast<uint64_t>(l.size());
        for (auto e : l) {
          (*write_element)(stream, static_cast<py::object &>(e), m, entry_metadata);
        }
      }
    },
  },

  {
    pycall::type_string<pycall::default_mapping, std::shared_ptr<pycall::tuple_wrapper>>(),
    {
      [](pycall::stream &stream, stream_module &m, const pycall::typestring_tree &metadata) {
        const auto entries_ts = metadata.entries();

        uint64_t length;
        stream >> length;
        py::print("tuple size: " + std::to_string(length));

        if (length != entries_ts.size())
          throw std::invalid_argument("size mismatch between the read tuple size and the given metadata");

        py::list list;
        for (size_t i = 0; i < entries_ts.size(); ++i) {
          const std::string &entry_ts = entries_ts[i].first;
          const pycall::typestring_tree &entry_metadata = entries_ts[i].second;

          stream_module::stream_read_func *read_element = m.find_stream_read(entry_ts);
          if (read_element == nullptr)
            throw std::invalid_argument("Missing stream-read function for typestring '" + entry_ts + "'");

          py::print("Read element of tuple from stream");
          // read elements of tuple
          list.append((*read_element)(stream, m, entry_metadata));
        }
        py::print("Before conversion");
        py::print(py::tuple(list));
        py::print("after conversion");
        return py::tuple(list);
      },
      [](pycall::stream &stream, py::object obj, stream_module &m, const pycall::typestring_tree &metadata) {
        const auto entries_ts = metadata.entries();
        py::list list = obj; // throws if this cannot convert (if obj is a tuple, this should work)

        if (entries_ts.size() != list.size())
          throw std::invalid_argument("size mismatch between the input tuple and the given metadata");

        stream << static_cast<uint64_t>(entries_ts.size());
        size_t i = 0;
        for (auto e : list) {
          const std::string &entry_ts = entries_ts[i].first;
          const pycall::typestring_tree &entry_metadata = entries_ts[i].second;

          stream_module::stream_write_func *write_element = m.find_stream_write(entry_ts);
          if (write_element == nullptr)
            throw std::invalid_argument("Missing stream-write function for typestring '" + entry_ts + "'");

          // write elements of tuple
          (*write_element)(stream, static_cast<py::object &>(e), m, entry_metadata);

          ++i;
        }
      }
    }
  }
};

stream_module modules::default_comm(default_function_map);
