#ifndef DEFAULT_COMM_H
#define DEFAULT_COMM_H

#include <pybind11/pybind11.h>
namespace py = pybind11;

#include <map>
#include <functional>
#include <string>
#include <pycall/core/pycall.hpp>

#include "stream_module.h"

struct modules {
  static stream_module default_comm;
};


#endif // DEFAULT_COMM_H
