#ifndef RECEIVER_H
#define RECEIVER_H

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
namespace py = pybind11;

#include <pycall/core/pycall.hpp>
#include <functional>
#include <map>

#include "default_comm.h"


class pyreceiver
{
public:
  pyreceiver(pycall::stream &stream, stream_module &sm);
  pyreceiver(pycall::stream &stream);
  void listen();

  bool error();
  std::string error_description();

private:
  void mark_error(const std::string &msg);
  void unmark_error();

private:
  pycall::stream &stream;
  stream_module &sm;
  bool error_occured = false;
  std::string error_msg = "";
};

void bind_pyreceiver(py::module &m);


#endif // RECEIVER_H
