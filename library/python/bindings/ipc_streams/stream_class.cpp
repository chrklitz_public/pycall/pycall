#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <pycall/core/pycall.hpp>

#include <pycall/core/binding_definitions.h>
#include <pycall/ipc_streams/interprocess_stream.h>

namespace py = pybind11;

void add_raw(interprocess_stream *stream, py::array_t<char> arr) {
  stream->add_raw(arr.mutable_data(0), arr.size());
}

py::array_t<char> read_raw(interprocess_stream *stream) {
  void *data;
  size_t len;
  stream->read_raw(&data, &len);

  py::str dummy_owner;
  return py::array_t<char>(
        {len},
        {sizeof(char)},
        static_cast<char *>(data),
        dummy_owner);
}

void bind_interprocessing_stream(py::module &m) {
  py::class_<pycall::stream>(m, "stream");

  py::class_<interprocess_stream, pycall::stream>(m, "interprocess_stream")
      .def(py::init<const std::string>())
      .def("add_raw", &add_raw)
      .def("read_raw", &read_raw)
      .def("send", &interprocess_stream::send)
      .def("receive", &interprocess_stream::receive)
      .def("key", &interprocess_stream::key)
      ;
}
