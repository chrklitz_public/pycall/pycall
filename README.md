# Note from the author
This project will be restructured very soon with the goal of a better usability and easier development.

# Pycall - remote procedure call

This project enables C++ processes to call python functions in a seperate process remotely without embedding the
interpreter. This allows multiple concurrent python function executions in any python version on different machines 
with a simple function-call syntax inside C++. 

## Supported features:
- C++ type-maps for supporting new types quickly
- template type connections (type-map)
- python void returns
- python non-void returns (all supported types in the type-map)

## Potential features:
- positional arguments


For now the interprocess communication is handled using shared memory which restricts the python process to run on the same system.
However this can be easily replaced with a communication with for example TCP which would enable to call python functions
on any system which installs this library and which installs the necessary bindings for custom types which should be supported.

# Installation
Will be added later

# Getting started
Take a look at the [tutorial](https://gitlab.com/chrklitz_public/pycall/pycall/-/wikis/Tutorial/Tutorial) in the gitlab wiki.

# Documentation
[wiki](https://gitlab.com/chrklitz_public/pycall/pycall/-/wikis/Tutorial/Tutorial) and doxygen ci-artifact

# License
See [here](LICENSE). If you use this project, then it would be nice if you reference this repository :)
